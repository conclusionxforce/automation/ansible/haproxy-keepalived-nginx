haproxy keepalived
=========

Install haproxy with keepalived

Requirements
------------

None.

Role Variables
--------------

keepalived_interval: 2
keepalived_virtual_router_id: 22
keepalived_interface: enp1s0
keepalived_MASTER_SLAVE: MASTER
keepalived_priority: 200
keepalived_virtualip: 192.168.122.10

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: loadbalancers
  gather_facts: false
  become: yes
  roles:
    - haproxy-keepalived
```

License
-------

MIT

Author Information
------------------

Jeroen van de Lockand
